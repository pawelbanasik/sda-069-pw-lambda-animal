package com.pawelbanasik;

public class Main {

	public static void main(String[] args) {

		Animal a = new Animal();
		a.addAnimal("Slon");
		a.addAnimal("Lew");
		a.addAnimal("kot_pies_bobr", anims -> anims.split("_"));
		a.addAnimal("jez|slowik", anims -> anims.split("\\|"));
		
		for (String anim : a.getAnimals()) {
		System.out.println(anim);
		}
		
		
		
	}

}
