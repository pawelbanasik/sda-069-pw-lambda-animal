package com.pawelbanasik;

import java.util.LinkedList;
import java.util.List;

public class Animal {

	private List<String> animals = new LinkedList<>();

	public void addAnimal(String animal) {
		animals.add(animal);
	}

	public void addAnimal(String animal, AnimalInterface ai) {

		String[] anims = ai.add(animal);

		for (String anim : anims) {
			this.animals.add(anim);
		}

	}

	public boolean removeAnimal(String animal) {

		if (animals.contains(animal)) {
			animals.remove(animal);
			return true;
		}
		return false;
		

	}

	public List<String> getAnimals() {
		return animals;
	}

}
